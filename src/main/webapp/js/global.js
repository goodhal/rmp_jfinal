var tableName = 'rmp';//本地存储表名
var version = 2.4;//用于更新本地缓存配置


var userKey = top.userKey;
var message = top.message;
if (top.templates != null)  var templates = top.templates;
var rmputils = top.rmputils;
var userSetting = top.userSetting;
var maxHeight = $(document).height();
var maxWidth = $(document).width();
/**
 * 自定义layui扩展模块
 */
layui.config({
    base: '../../js/ext/'
});

/**
 * 处理datatables错误显示
 */
try {
	$.fn.dataTable.ext.errMode = 'throw';
} catch (e) {

}

/**
 * 请求url
 */

var VALIDATE_USER_KEY_URL = 'config/login';
var USER_LOGOUT_URL = 'config/logout';
var LIST_USER_SPACE_INFO_URL = 'config/listSpace';
var GET_USER_SPACE_SETTING_URL = 'config/getUserSetting';
var USER_SPACE_ADD_KEY_URL = "config/add";


var LOGIN_BY_TOKEN_URL = "config/loginByToken";

var SETTING_UPDATE_URL = "../../config/updateSetting";
var GET_USER_CUSTOM_LINUX_COMMAND = "../../config/getCustomLinuxCommandSetting";
var UPDATE_USER_CUSTOM_LINUX_COMMAND = "../../config/updateCustomLinuxCommandSetting";
var RESET_DEFAULT_LINUX_COMMAND = "../../config/resetDefaultLinuxCommand";

var SERVER_INFO_LIST_ALL_URL = '../../server/listAll';
var SERVER_INFO_DEL_URL = '../../server/del';
var SERVER_INFO_EDIT_URL = '../../server/edit';
var SERVER_INFO_MONITORING_URL = '../../server/monitoring';
var SERVER_INFO_BATCH_SAVE_URL = '../../server/batchSave';


var MONITORING_SERVER_LIST_URL = 'list';
var MONITORING_SERVER_DEL_URL = 'del';
var MONITORING_SERVER_RECONNECT_URL = 'reconnect';
var MONITORING_SERVER_DEL_ALL_URL = 'delAll';

var MONITORING_SERVER_ADD_JVM_URL = 'addJvm';
var MONITORING_SERVER_CHECK_LINUX_JPS_URL = "checkJps";
var MONITORING_SERVER_SHOW_STACK_INFO_URL = "stack";

var MONITORING_SERVER_SEND_COMMAND_URL = "execCommand";
var MONITORING_SERVER_BREAK_COMMAND_URL = "breakCommand";

var MONITORING_SERVER_SAVE_DATA_URL = '../../report/saveData';

var REPORT_HISTORY_FILE_LIST_URL = "../../report/list";
var REPORT_HISTORY_FILE_DEL_URL = "../../report/del";
var REPORT_HISTORY_ANALYZE_EXCEL_EXPORT_URL = "../../report/exportExcel";
var REPORT_HISTORY_GET_INFO_DATA_URL = "../../report/getInfoData";

var REPORT_HISTORY_ANALYZE_API_DATA_URL = "../../report/analyzeApiData";
var REPORT_HISTORY_SAVE_API_DATA_URL = "../../report/saveApiData";
var REPORT_HISTORY_SEND_API_DATA_URL = "../../report/sendApiData";
var REPORT_HISTORY_DEL_API_DATA_URL = "../../report/delApiData";
var REPORT_HISTORY_LIST_API_DATA_HISTORY_URL = "../../report/listApiDataHistory";
var REPORT_HISTORY_API_DATA_VALIDATE_FILE_INFO_URL= "../../report/validateFileInfo";
var REPORT_HISTORY_CREATE_FINAL_API_DATA_JSON_URL = "../../report/createFinalJson";

/**
 * 默认标记css className
 */
var DYNAMIC_DATA_CLASS = "dynamic-data"; //动态数据
var SERVER_CONNECT_STATUS_CLASS = "server-connect-status";//链接状态标记
var SERVER_ACTION_BAR_BTN_RECONNECT_CLASS = "rpm-monitoring-btn-reconnect";//重连
var SERVER_ACTION_BAR_BTN_DEL_CLASS = "rpm-monitoring-btn-del";//删除按钮


//通用Datatables配置
var DT_CONSTANT = {
    DATA_TABLES : {
        DEFAULT_OPTION:{
            "aaSorting": [[ 1, "asc" ]],//默认第几个排序
            "bStateSave": true,//状态保存
            "processing": false,   //显示处理状态
            "serverSide": false,  //服务器处理
            "autoWidth": false,   //自动宽度
            "scrollX": true,
            "lengthChange": true,
            "paging": true,
            "language": {
                "url": "../../plugins/zh_CN.json"
            },
            "lengthMenu": [[10, 15, 20, 50, 9999999], ['10', '15', '20', '50','全部']],  //显示数量设置
            "initComplete": function(settings, json){ //多选框的设置
                $("th > input[type='checkbox']:eq(0)").click(function() {
                	$(this).parents('.dataTables_wrapper').find("td > input[type='checkbox']").prop("checked", $(this).is(":checked"));
                });
            }
        },
        //常用的COLUMN render
        COLUMNFUN:{
            //过长内容省略号替代
            ELLIPSIS:function (data, type, row, meta) {
                data = data||"";
                return '<span title="' + data + '" class="layui-elip">' + data + '</span>';
            },
            TAGS:function(data, type, row, meta){
            	if (data == null || data == "") {
            		return "";
            	}
                var arrs = data.split("\\s+");
                var html = '';
                $.each(arrs, function(i, n) {
                    html += '<span class="layui-badge layui-bg-green rmp-server-info-tags-label">' + n + '</span>&nbsp;';

                });
                return html;
            }
        },
        //html渲染工具
        RENDERUTIL:{
            LABEL_STATUS:function(option, data){
            	if (data == null || data.length == 0) {
            		return "";
            	}
                //可选orange green default
                var color = option[data] ? option[data] : option['default'];

                return '<span class="layui-badge layui-bg-' + color + '">' + data + '</span>';
            },
            DYNAMIC_DATA:function(dataId, serverType, dataItem, dataName, data){
                return '<span class="' + DYNAMIC_DATA_CLASS + ' layui-badge layui-bg-cyan" data-id="'
                    + dataId + '" data-item="' + dataItem + '" data-name="' + dataName
                    + '" server-type="' + serverType + '">&nbsp;' + data + '&nbsp;</span>';
            },
            COMPLEX_DYNAMIC_DATA:function(dataId, serverType, dataItem, dataName, data){
                var html = '<span class="' + DYNAMIC_DATA_CLASS + '" data-id="'
                    + dataId + '" data-item="' + dataItem + '" data-name="' + dataName
                    + '" server-type="' + serverType + '"> ';
                var count = 0;
                $.each(data, function(name, info){
                    (count > 0 && count % 2 == 0) && (html += '<br>');
                    html += '<span class="layui-badge layui-bg-cyan">&nbsp;&nbsp;' + name + ':&nbsp;' + info[dataName] + '&nbsp;&nbsp;</span>&nbsp;&nbsp;';
                    count++;
                });
                html += '</span>';
                return html;
            },
            CONNECT_STATUS:function(connectStatus){
                if (connectStatus != "true") {
                    return '<a href="javascript:;" onclick="javascript:layer.alert(\'' + connectStatus + '\',{icon:5});">' +
                        '<span class="layui-badge ' + SERVER_CONNECT_STATUS_CLASS + '">异常</span></a>';
                }
                return '<span class="layui-badge layui-bg-green ' + SERVER_CONNECT_STATUS_CLASS + '">正常</span>';
            },
            ACTION_BAR:function(dataId, serverType, btns){
                var html = '';
                $.each(btns, function(text, className){
                    html += '<button type="button" class="layui-btn layui-btn-danger layui-btn-sm ' + className + '" server-type="' + serverType + '" data-id="' + dataId + '">' + text + '</button>';
                });
                return html;
            }
        }
    }
};


/**
 * 提前定义好不同类型服务器信息<br>
 * 通过该配置使用JS渲染页面
 */
var serverInfos = {
    //可供选择的类型，在服务器管理模块使用
    serverTypes:{
        linux:"Linux",
        weblogic:"Weblogic",
        tomcat:"Tomcat",
        dubbo:"Dubbo",
        redis:"Redis"
    },
    //可以监控视图上显示的类型,视图显示已在此定义的类型为准
    viewTypes:{
        linux:"Linux",
        weblogic:"Weblogic",
        jvm:"JVM",
        tomcat:"Tomcat",
        dubbo:"Dubbo",
        redis:"Redis"
/*      nginx:"Nginx",
        redis:"Redis",
        mongodb:"MongoDB",
        dubbo:"Dubbo"*/
    },
    redis:{
        namespace:"../../redis/",
        additionalParameters:{
            mode:{
                mark:'连接模式',
                placeholder:'默认local本地客户端连接，需开启远程访问权限',
                defaultValue:'local'
            },
            linuxLoginUsername:{
                mark:'主机账号',
                placeholder:'对应主机的登录账号,如果为local模式则不用填写',
                defaultValue:''
            },
            linuxLoginPassword:{
                mark:'主机密码',
                placeholder:'对应主机的登录密码,如果为local模式则不用填写',
                defaultValue:''
            },
            linuxLoginPort:{
            	mark:'主机登录端口',
                placeholder:'对应主机的SSH端口',
                defaultValue:'22'
            },
            redisCliPath:{
            	mark:'redis-cli绝对路径',
                placeholder:'目标主机上的redis-cli的绝对路径',
                defaultValue:''
            }
        },
        dtInitParameter:{
            ajax:{
                url:"../../redis/" + MONITORING_SERVER_LIST_URL,
                data:{userKey:userKey, serverType:"redis"}
            },
            paging:false,
            columnDefs: [{"orderable":false, "aTargets":[0, 24]}],
            columns:[
                {
                    "data": null,
                    "render": function(data) {
                        return '<input type="checkbox" data-id="' + data.viewId + '" server-type="' + data.serverType + '">';
                    }
                },
                {
                    "data":"viewId"
                },
                {
                    "data":null,
                    "render":function(data, type, full, meta) {
                        return data.host + ":" + data.port;
                    }
                },
                {
                    "data":"realHost",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"tags",
                    "render":DT_CONSTANT.DATA_TABLES.COLUMNFUN.TAGS
                },
                {
                    "data":"username",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                   "data" :"info.version",
                   "render":function(data, type, full, meta){
                       return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                           "default":"green"
                       }, data);
                   }
                },
                {
                    "data" :"info.role",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                            "default":"blue",
                            "master":""
                        }, data);
                    }
                },
                {
                    "data" :"info.processId"
                },
                {
                    "data" :"info.upTime",
                    "render":function(data, type, full, meta) {
                        return data + "天";
                    }
                },
                {
                    "data" :"info.clientCount",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "clientCount", data);
                    }
                },
                {
                    "data" :"info.usedMemory",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "usedMemory", data + "kb");
                    }
                },
                {
                    "data" :"info.usedMemoryMax",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "usedMemoryMax", data + "kb");
                    }
                },
                {
                    "data" :"info.usedCpuSys",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "usedCpuSys", data + "%");
                    }
                },
                {
                    "data" :"info.usedCpuUser",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "usedCpuUser", data + "%");
                    }
                },
                {
                    "data" :"info.usedCpu",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "usedCpu", data + "%");
                    }
                },
                {
                    "data" :"info.totalPercentSecondCurrent",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "totalPercentSecondCurrent", data);
                    }
                },
                {
                    "data" :"info.keyspaceHits",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "keyspaceHits", data);
                    }
                },
                {
                    "data" :"info.keyspaceMisses",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "keyspaceMisses", data);
                    }
                },             
                {
                    "data" :"info.keyspaceHitRateCurrent",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "keyspaceHitRateCurrent", data + "%");
                    }
                },
                {
                    "data" :"info.expiredKeys",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "expiredKeys", data);
                    }
                },
                {
                    "data" :"info.evictedKeys",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "evictedKeys", data);
                    }
                },
                {
                    "data" :"info.dbKeyCount",
                    "render":function(data, type, full, meta) {
                    	return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'redis',"commonInfo", "dbKeyCount", data);
                    }
                }, 
                {
                    "data":"connectStatus",
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.CONNECT_STATUS(data);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.ACTION_BAR(data.viewId, "redis", {
                        	"实时读写":"rpm-monitoring-btn-redis-stat", 
                            "删除":SERVER_ACTION_BAR_BTN_DEL_CLASS,
                            "重连":SERVER_ACTION_BAR_BTN_RECONNECT_CLASS                           
                        });
                    }
                }
            ]
        },
        tableColumn:{
            "1":"ID",
            "2":"HOST",
            "3":"真实HOST",
            "4":"标签",
            "5":"用户名",
            "6":"redis版本号",
            "7":"实例角色",
            "8":"进程号",
            "9":"运行时间",
            "10":"连接数",
            "11":"分配内存大小",
            "12":"最大<br>分配内存大小",
            "13":"系统<br>CPU使用",
            "14":"用户<br>CPU使用",
            "15":"CPU使用",
            "16":"实时QPS",
            "17":"命中key<br>的数量",
            "18":"未命中key<br>数量",
            "19":"实时命中<br>成功率",
            "20":"过期key<br>总数量",
            "21":"删除key<br>总数量",
            "22":"key数量",
            "23":"连接状态",
            "24":"操作"
        },
        btnTool:[
            {
				size:'sm',
				type:'default',
				text:'Redis客户端',
				markClass:"rmp-monitoring-console"
			}],
        alertSettingValue:{
        	usedCpu:{
                value:3600,
                sign:"%",
                mode:">"
            },
            keyspaceHitRateCurrent:{
                value:50,
                sign:"",
                mode:"<"
            },
            totalPercentSecondCurrent:{
                value:5000000,
                sign:"",
                mode:">"
            }
        },
        constant:{
        	clientCount:"连接数",
        	usedMemory:"分配内存大小[KB]",
        	usedMemoryMax:"最大分配内存大小[KB]",
        	usedCpuSys:"CPU消耗-系统[%]",
        	usedCpuUser:"CPU消耗-用户[%]",
        	usedCpu:"CPU消耗[%]",
        	totalPercentSecondCurrent:"实时QPS",
        	keyspaceHits:"命中key总数量",
        	keyspaceMisses:"未命中key总数量",
        	keyspaceHitRateCurrent:"实时命中成功率[%]",
        	expiredKeys:"失效key总数量",
        	evictedKeys:"删除key总数量",
        	dbKeyCount:"key数量"
        },
        propertyObject:{
            commonInfo:{
            	clientCount:[],
            	usedMemory:[],
            	usedMemoryMax:[],
            	usedCpuSys:[],
            	usedCpuUser:[],
            	usedCpu:[],
            	totalPercentSecondCurrent:[],
            	keyspaceHits:[],
            	keyspaceMisses:[],
            	keyspaceHitRateCurrent:[],
            	expiredKeys:[],
            	evictedKeys:[],
            	dbKeyCount:[]
            }
        }
    },
    dubbo:{
        namespace:"../../dubbo/",
        additionalParameters:{
            javaHome:{
                mark:'javaHome',
                placeholder:'java安装目录',
                defaultValue:'/opt/jdk1.7'
            },
            linuxLoginUsername:{
                mark:'主机账号',
                placeholder:'对应主机的登录账号',
                defaultValue:''
            },
            linuxLoginPassword:{
                mark:'主机密码',
                placeholder:'对应主机的登录密码',
                defaultValue:''
            },
            startScriptPath:{
                mark:'启动脚本路径',
                placeholder:'Dubbo的启动脚本完整路径',
                defaultValue:''
            }
        },
        dtInitParameter:{
            ajax:{
                url:"../../dubbo/" + MONITORING_SERVER_LIST_URL,
                data:{userKey:userKey, serverType:"dubbo"}
            },
            paging:false,
            columnDefs: [{"orderable":false, "aTargets":[0, 24]}],
            columns:[
                {
                    "data": null,
                    "render": function(data) {
                        return '<input type="checkbox" data-id="' + data.viewId + '" server-type="' + data.serverType + '">';
                    }
                },
                {
                    "data":"viewId"
                },
                {
                    "data":null,
                    "render":function(data, type, full, meta) {
                        return data.host + ":" + data.port;
                    }
                },
                {
                    "data":"realHost",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"tags",
                    "render":DT_CONSTANT.DATA_TABLES.COLUMNFUN.TAGS
                },
                {
                    "data":"username",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                   "data" :"info.threadStatus",
                   "render":function(data, type, full, meta){
                       return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                           "OK":"green",
                           "default":"red"
                       }, data);
                   }
                },
                {
                    "data" :"info.threadMaxCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "threadMaxCount", data);
                    }
                },
                {
                    "data" :"info.threadCoreCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "threadCoreCount", data);
                    }
                },
                {
                    "data" :"info.threadLargestCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "threadLargestCount", data);
                    }
                },
                {
                    "data" :"info.threadActiveCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "threadActiveCount", data);
                    }
                },
                {
                    "data" :"info.threadTaskCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "threadTaskCount", data);
                    }
                },
                {
                    "data" :"info.memoryStatus",
                    "render":function(data, type, full, meta){
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                            "OK":"green",
                            "default":"red"
                        }, data);
                    }
                 },
                 {
                     "data" :"info.memoryMax",
                     "render":function(data, type, full, meta) {
                         return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "memoryMax", data + "MB");
                     } 
                 },
                 {
                     "data" :"info.memoryTotal",
                     "render":function(data, type, full, meta) {
                         return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "memoryTotal", data + "MB");
                     } 
                 },
                 {
                     "data" :"info.memoryUsed",
                     "render":function(data, type, full, meta) {
                         return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "memoryUsed", data + "MB");
                     } 
                 },
                 {
                     "data" :"info.memoryFree",
                     "render":function(data, type, full, meta) {
                         return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "memoryFree", data + "MB");
                     } 
                 },
                 {
                     "data" :"info.memoryUsedPercent",
                     "render":function(data, type, full, meta) {
                         return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'dubbo',"commonInfo", "memoryUsedPercent", data + "%");
                     } 
                 },
                 {
                     "data" :"info.loadStatus",
                     "render":function(data, type, full, meta){
                         return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                             "OK":"green",
                             "default":"red"
                         }, data);
                     }
                  },
                  {
                      "data" :"info.serverStatus",
                      "render":function(data, type, full, meta){
                          return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                              "OK":"green",
                              "default":"red"
                          }, data);
                      }
                   },
                   {
                       "data" :"info.registryStatus",
                       "render":function(data, type, full, meta){
                           return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                               "OK":"green",
                               "default":"red"
                           }, data);
                       }
                    },
                    {
                        "data" :"info.springStatus",
                        "render":function(data, type, full, meta){
                            return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                                "OK":"green",
                                "default":"red"
                            }, data);
                        }
                     },
                     {
                         "data" :"info.summaryStatus",
                         "render":function(data, type, full, meta){
                             return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                                 "OK":"green",
                                 "default":"red"
                             }, data);
                         }
                      },             
                {
                    "data":"connectStatus",
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.CONNECT_STATUS(data);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.ACTION_BAR(data.viewId, "dubbo", {
                        	"JVM":"rpm-monitoring-btn-other-jvm", 
                            "删除":SERVER_ACTION_BAR_BTN_DEL_CLASS,
                            "重连":SERVER_ACTION_BAR_BTN_RECONNECT_CLASS                           
                        });
                    }
                }
            ]
        },
        tableColumn:{
            "1":"ID",
            "2":"HOST",
            "3":"真实HOST",
            "4":"标签",
            "5":"用户名",
            "6":"线程池状态",
            "7":"线程<br>最大维护数量",
            "8":"线程<br>最小维护数量",
            "9":"线程<br>历史最大",
            "10":"线程<br>当前活跃",
            "11":"线程任务数",
            "12":"内存状态",
            "13":"内存<br>最大值",
            "14":"内存<br>当前可使用",
            "15":"内存<br>当前使用",
            "16":"内存<br>当前空闲",
            "17":"内存<br>当前使用百分比",
            "18":"Load状态",
            "19":"Server状态",
            "20":"Registry状态",
            "21":"spring状态",
            "22":"汇总状态",
            "23":"连接状态",
            "24":"操作"
        },
        btnTool:[
            {
				size:'sm',
				type:'default',
				text:'Telnet控制台',
				markClass:"rmp-monitoring-console"
			}],
        alertSettingValue:{
        	memoryUsedPercent:{
                value:100,
                sign:"%",
                mode:">"
            },
            threadActiveCount:{
                value:500,
                sign:"",
                mode:">"
            }
        },
        constant:{
        	threadMaxCount:"线程最大维护数量",
        	threadCoreCount:"线程最小维护数量",
        	threadLargestCount:"历史最大线程使用",
        	threadActiveCount:"当前活跃线程数",
        	threadTaskCount:"线程任务数",
        	memoryMax:"最大内存[MB]",
        	memoryTotal:"全部可使用内存[MB]",
        	memoryUsed:"当前使用内存[MB]",
        	memoryFree:"当前空闲内存[MB]",
        	memoryUsedPercent:"当前内存使用百分比[%]"
        },
        propertyObject:{
            commonInfo:{
            	threadMaxCount:[],
            	threadCoreCount:[],
            	threadLargestCount:[],
            	threadActiveCount:[],
            	threadTaskCount:[],
            	memoryMax:[],
            	memoryTotal:[],
            	memoryUsed:[],
            	memoryFree:[],
            	memoryUsedPercent:[]
            }
        }
    },
    tomcat:{
        namespace:"../../tomcat/",
        additionalParameters:{
        	webPort:{
        		mark:'监听端口',
                placeholder:'web监听端口',
                defaultValue:'8080'
        	},
        	projectName:{
        		mark:'项目路径',
                placeholder:'需要监控的项目路径',
                defaultValue:'/'
        	},      	
            javaHome:{
                mark:'javaHome',
                placeholder:'java安装目录',
                defaultValue:'/opt/jdk1.7'
            },
            linuxLoginUsername:{
                mark:'主机账号',
                placeholder:'对应主机的登录账号',
                defaultValue:''
            },
            linuxLoginPassword:{
                mark:'主机密码',
                placeholder:'对应主机的登录密码',
                defaultValue:''
            },
            startScriptPath:{
                mark:'启动脚本路径',
                placeholder:'该weblogic的启动脚本完整路径',
                defaultValue:''
            }
        },
        dtInitParameter:{
            ajax:{
                url:"../../tomcat/" + MONITORING_SERVER_LIST_URL,
                data:{userKey:userKey, serverType:"tomcat"}
            },
            paging:false,
            columnDefs: [{"orderable":false, "aTargets":[0, 17]}],
            columns:[
                {
                    "data": null,
                    "render": function(data) {
                        return '<input type="checkbox" data-id="' + data.viewId + '" server-type="' + data.serverType + '">';
                    }
                },
                {
                    "data":"viewId"
                },
                {
                    "data":null,
                    "render":function(data, type, full, meta) {
                        return data.host + ":" + data.port;
                    }
                },
                {
                    "data":"realHost",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"tags",
                    "render":DT_CONSTANT.DATA_TABLES.COLUMNFUN.TAGS
                },
                {
                    "data":"username",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                   "data" :"info.startTime"
                },
                {
                    "data" :"info.upTime"
                },
                {
                    "data" :"info.runType"
                },
                {
                    "data" :"info.webPort"
                },
                {
                	"data":"info.projectPath"
                },
                {
                    "data" :"info.sessionMaxActiveCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'tomcat',"commonInfo", "sessionMaxActiveCount", data);
                    }
                },
                {
                    "data" :"info.sessionActiveCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'tomcat',"commonInfo", "sessionActiveCount", data);
                    }
                },
                {
                    "data" :"info.sessionCounter",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'tomcat',"commonInfo", "sessionCounter", data);
                    }
                },
                {
                    "data" :"info.threadMaxCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'tomcat',"commonInfo", "threadMaxCount", data);
                    }
                },
                {
                    "data" :"info.threadCurrentCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'tomcat',"commonInfo", "threadCurrentCount", data);
                    }
                },
                {
                    "data" :"info.threadCurrentBusyCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'tomcat',"commonInfo", "threadCurrentBusyCount", data);
                    }
                },
                {
                    "data":"connectStatus",
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.CONNECT_STATUS(data);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.ACTION_BAR(data.viewId, "tomcat", {
                        	"JVM":"rpm-monitoring-btn-other-jvm", 
                            "删除":SERVER_ACTION_BAR_BTN_DEL_CLASS,
                            "重连":SERVER_ACTION_BAR_BTN_RECONNECT_CLASS
                        });
                    }
                }
            ]
        },
        tableColumn:{
            "1":"ID",
            "2":"HOST",
            "3":"真实HOST",
            "4":"标签",
            "5":"用户名",
            "6":"开启时间",
            "7":"工作时长",
            "8":"运行模式",
            "9":"监听端口",
            "10":"项目路径",
            "11":"最大会话数",
            "12":"会话数",
            "13":"活动会话数",
            "14":"最大线程数",
            "15":"当前线程数",
            "16":"当前繁忙线程数",           
            "17":"连接状态",
            "18":"操作"
        },
        btnTool:[],
        alertSettingValue:{
        	threadCurrentBusyCount:{
                value:10,
                sign:"",
                mode:">"
            }
        },
        constant:{
        	sessionMaxActiveCount:"Session最大会话数",
        	sessionActiveCount:"Session会话数",
        	sessionCounter:"Session活动会话数",
        	threadMaxCount:"最大线程数",
        	threadCurrentCount:"当前线程数",
        	threadCurrentBusyCount:"当前繁忙线程数"
        },
        propertyObject:{
            commonInfo:{
            	sessionMaxActiveCount:[],
            	sessionActiveCount:[],
            	sessionCounter:[],
            	threadMaxCount:[],
            	threadCurrentCount:[],
            	threadCurrentBusyCount:[]
            }
        }   
    },
    jvm:{
    	namespace:"../../jvm/",
        additionalParameters:{ //附加参数设置
            javaHome:{
                mark:'javaHome',
                placeholder:'java安装目录',
                defaultValue:'/opt/jdk1.7'
            }
        },
        //dt初始化参数
        dtInitParameter:{
            ajax:{
                url:"../../jvm/" + MONITORING_SERVER_LIST_URL,
                data:{userKey:userKey, serverType:"jvm"}
            },
            paging:false,
            columnDefs: [{"orderable":false, "aTargets":[0, 20]}],
            columns:[
                {
                    "data": null,
                    "render": function(data) {
                        return '<input type="checkbox" data-id="' + data.viewId + '" server-type="' + data.serverType + '">';
                    }
                },
                {
                    "data":"viewId"
                },
                {
                    "data":null,
                    "render":function(data, type, full, meta) {
                        return data.host + ":" + data.port;
                    }
                },
                {
                    "data":"realHost",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"tags",
                    "render":DT_CONSTANT.DATA_TABLES.COLUMNFUN.TAGS
                },
                {
                    "data":"username",
                    "render":function(data) {
                    	return data || "";
                    }
                },  
                {
                	"data":"pid"
                },
                {
                	"data":"processName"
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "survivorSpacePercent_0", data.info.survivorSpacePercent_0 + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "survivorSpacePercent_1", data.info.survivorSpacePercent_1 + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "edenSpacePercent", data.info.edenSpacePercent + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "oldSpacePercent", data.info.oldSpacePercent + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "permSpacePercent", data.info.permSpacePercent + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "youngGCTotalCount", data.info.youngGCTotalCount);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "youngGCTime", data.info.youngGCTime + "s");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "fullGCTotalCount", data.info.fullGCTotalCount);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "fullGCTime", data.info.fullGCTime + "s");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "gcTotalTime", data.info.gcTotalTime + "s");
                    }
                },
                {
                	"data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'jvm',"commonInfo", "blockedThreadCount", data.info.blockedThreadCount);
                    }
                },
                {
                    "data":"connectStatus",
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.CONNECT_STATUS(data);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.ACTION_BAR(data.viewId, "jvm", {  
                        	"堆栈":"rmp-monitoring-jvm-stack",                       
                            "删除":SERVER_ACTION_BAR_BTN_DEL_CLASS,
                            "重连":SERVER_ACTION_BAR_BTN_RECONNECT_CLASS
                        });
                    }
                }
            ]
        },
        //表头列设定
        tableColumn:{
            "1":"ID",
            "2":"HOST",
            "3":"真实HOST",
            "4":"标签",
            "5":"用户名",
            "6":"进程PID",
            "7":"进程名",
            "8":"Survivor_0",
            "9":"Survivor_1",
            "10":"Eden",
            "11":"Old",
            "12":"Perm",
            "13":"YoungGCCount",
            "14":"YoungGCTime",
            "15":"FullGCCount",
            "16":"FullGCTime",
            "17":"GCTotalTime",
            "18":"BLOCKED线程数",
            "19":"连接状态",
            "20":"操作"
        },
        //表头上方工具栏设定 只限button
        btnTool:[
/*            {
                size:'sm',
                type:'default',
                text:'命令控制台',
                id:"rmp-monitoring-linux-console"
            }*/
        ],
        //预警信息值设定
        alertSettingValue:{
        	edenSpacePercent:{
    			value:100,
    			sign:"%",
    			mode:">"
    		},
    		oldSpacePercent:{
    			value:100,
    			sign:"%",
    			mode:">"
    		},
    		permSpacePercent:{
    			value:100,
    			sign:"%",
    			mode:">"
    		},
    		blockedThreadCount:{
    			value:10,
    			sign:"",
    			mode:">"
    		}
        },
        //对应常量解释
        constant:{
        	survivorSpacePercent_0:"幸存0区容量使用百分比[%]",
			survivorSpacePercent_1:"幸存1区容量使用百分比[%]",
			edenSpacePercent:"伊甸园区容量使用百分比[%]",
			oldSpacePercent:"老生代容量使用百分比[%]",
			permSpacePercent:"持久代容量使用百分比[%]",
			youngGCTotalCount:"Young GC总次数",
			youngGCTime:"Young GC花费总时间[s]",
			fullGCTotalCount:"Full GC总次数",
			fullGCTime:"Full GC花费总时间[s]",
			gcTotalTime:"GC花费的总时间[s]",
			blockedThreadCount:"BLOCKED线程数量"
        },
        //类型属性定义
        propertyObject:{
        	commonInfo:{
    			survivorSpacePercent_0:[],
    			survivorSpacePercent_1:[],
    			edenSpacePercent:[],
    			oldSpacePercent:[],
    			permSpacePercent:[],
    			youngGCTotalCount:[],
    			youngGCTime:[],
    			fullGCTotalCount:[],
    			fullGCTime:[],
    			gcTotalTime:[],
    			blockedThreadCount:[]
    		}
        }
    },
    linux:{
        namespace:"../../linux/",
        additionalParameters:{ //附加参数设置
            javaHome:{
                mark:'javaHome',
                placeholder:'java安装目录',
                defaultValue:'/opt/jdk1.7'
            },
            networkCardName: {
                mark: '要监控的网卡名称,多个以逗号分隔',
                placeholder:'网卡名称,多个以逗号分隔',
                defaultValue:''
            }
        },
        //dt初始化参数
        dtInitParameter:{
            ajax:{
                url:"../../linux/" + MONITORING_SERVER_LIST_URL,
                data:{userKey:userKey, serverType:"linux"}
            },
            paging:false,
            columnDefs: [{"orderable":false, "aTargets":[0, 22]}],
            columns:[
                {
                    "data": null,
                    "render": function(data) {
                        return '<input type="checkbox" data-id="' + data.viewId + '" server-type="' + data.serverType + '">';
                    }
                },
                {
                    "data":"viewId"
                },
                {
                    "data":null,
                    "render":function(data, type, full, meta) {
                        return data.host + ":" + data.port;
                    }
                },
                {
                    "data":"realHost",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"tags",
                    "render":DT_CONSTANT.DATA_TABLES.COLUMNFUN.TAGS
                },
                {
                    "data":"username",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"cpuInfo",
 										
                    "render":function(data) {
                        return data + "核";
                    }
                },
                {
                    "data":"memInfo",
                    "render":function(data) {
                        return data + "kb";
                    }
                },
                {
                    "data":"info.diskInfo.userDisk",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'linux',"diskInfo", "userDisk", data + "%");
                    }
                },
                {
                    "data":"info.diskInfo.rootDisk",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'linux',"diskInfo", "rootDisk", data + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"commonInfo", "freeCpu", data.info.freeCpu + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"commonInfo", "freeMem", data.info.freeMem + "%");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return data.info.ioInfo == null ? '' : DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"ioInfo", "ioRead", data.info.ioInfo.ioRead + "MB/s");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return data.info.ioInfo == null ? '' : DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"ioInfo", "ioWrite", data.info.ioInfo.ioWrite + "MB/s");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"tcpInfo", "ESTABLISHED", data.info.tcpInfo.ESTABLISHED);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"tcpInfo", "CLOSE_WAIT", data.info.tcpInfo.CLOSE_WAIT);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"tcpInfo", "TIME_WAIT", data.info.tcpInfo.TIME_WAIT);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"tcpInfo", "LISTEN", data.info.tcpInfo.LISTEN);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"networkInfo", "rx", data.info.networkInfo.rx + "kb/s");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"networkInfo", "tx", data.info.networkInfo.tx + "kb/s");
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(data.viewId, 'linux',"commonInfo", "ioWait", data.info.ioWait + "%");
                    }
                },
                {
                    "data":"connectStatus",
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.CONNECT_STATUS(data);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.ACTION_BAR(data.viewId, "linux", {
                        	"JVM":"rpm-monitoring-btn-linux-jvm",
                            "删除":SERVER_ACTION_BAR_BTN_DEL_CLASS,
                            "重连":SERVER_ACTION_BAR_BTN_RECONNECT_CLASS
                        });
                    }
                }
            ]
        },
        //表头列设定
        tableColumn:{
            "1":"ID",
            "2":"HOST",
            "3":"真实HOST",
            "4":"标签",
            "5":"用户名",
            "6":"CPU核数",
            "7":"内存大小",
            "8":"磁盘空间<br>用户目录",
            "9":"磁盘空间<br>根目录",
            "10":"实时空闲CPU",
            "11":"实时空闲内存",
            "12":"IO读",
            "13":"IO写",
            "14":"TCP连接<br>ESTABLISHED",
            "15":"TCP连接<br>CLOSE_WAIT",
            "16":"TCP连接<br>TIME_WAIT",
            "17":"TCP连接<br>LISTEN",
            "18":"入网带宽",
            "19":"出网带宽",
            "20":"io等待",
            "21":"连接状态",
            "22":"操作"
        },
        //表头上方工具栏设定 只限button
        btnTool:[
            {
                size:'sm',
                type:'default',
                text:'SSH控制台',
                markClass:"rmp-monitoring-console"
            }
        ],
        //预警信息值设定
        alertSettingValue:{
            freeCpu:{
                value:10,
                sign:"%",
                mode:"<"
            },
            freeMem:{
                value:10,
                sign:"%",
                mode:"<"
            },
            userDisk:{
                value:90,
                sign:"%",
                mode:">"
            },
            ioWait:{
                value:20,
                sign:"%",
                mode:">"
            },
            TIME_WAIT:{
                value:40000,
                sign:"",
                mode:">"
            },
            CLOSE_WAIT:{
                value:40000,
                sign:"",
                mode:">"
            }
        },
        //对应常量解释
        constant:{
            ESTABLISHED:"Tcp连接-ESTABLISHED",
            CLOSE_WAIT:"Tcp连接-CLOSE_WAIT",
            LISTEN:"Tcp连接-LISTEN",
            TIME_WAIT:"Tcp连接-TIME_WAIT",
            rx:"入网总流量[kb/s]",
            tx:"出网总流量[kb/s]",
            rootDisk:"根目录磁盘已使用百分比[%]",
            userDisk:"用户目录磁盘已使用百分比[%]",
            freeCpu:"空闲CPU百分比[%]",
            freeMem:"空闲内存百分比[%]",
            ioWait:"io等待CPU执行时间百分比[%]",
            ioRead:"全部-IO读[MB/S]",
            ioWrite:"全部-IO写[MB/S]"
        },
        //类型属性定义
        propertyObject:{
            commonInfo:{
                freeCpu:[],
                freeMem:[],
                ioWait:[]
            },
            tcpInfo:{
                ESTABLISHED:[],
                LISTEN:[],
                CLOSE_WAIT:[],
                TIME_WAIT:[]
            },
            networkInfo:{
                rx:[],
                tx:[]
            },
            diskInfo:{
                rootDisk:[],
                userDisk:[]
            },
            ioInfo:{
            	ioRead:[],
            	ioWrite:[]
            }
        }

    },
    weblogic:{
        namespace:"../../weblogic/",
        additionalParameters:{
            javaHome:{
                mark:'javaHome',
                placeholder:'java安装目录',
                defaultValue:'/opt/jdk1.7'
            },
            linuxLoginUsername:{
                mark:'主机账号',
                placeholder:'对应主机的登录账号',
                defaultValue:''
            },
            linuxLoginPassword:{
                mark:'主机密码',
                placeholder:'对应主机的登录密码',
                defaultValue:''
            },
            startScriptPath:{
                mark:'启动脚本路径',
                placeholder:'该weblogic的启动脚本完整路径',
                defaultValue:''
            }
        },
        dtInitParameter:{
            ajax:{
                url:"../../weblogic/" + MONITORING_SERVER_LIST_URL,
                data:{userKey:userKey, serverType:"weblogic"}
            },
            paging:false,
            columnDefs: [{"orderable":false, "aTargets":[0, 19, 20, 21, 22, 23, 25]}],
            columns:[
                {
                    "data": null,
                    "render": function(data) {
                        return '<input type="checkbox" data-id="' + data.viewId + '" server-type="' + data.serverType + '">';
                    }
                },
                {
                    "data":"viewId"
                },
                {
                    "data":null,
                    "render":function(data, type, full, meta) {
                        return data.host + ":" + data.port;
                    }
                },
                {
                    "data":"realHost",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                    "data":"tags",
                    "render":DT_CONSTANT.DATA_TABLES.COLUMNFUN.TAGS
                },
                {
                    "data":"username",
                    "render":function(data) {
                    	return data || "";
                    }
                },
                {
                   "data" :"info.serverName"
                },
                {
                    "data" :"info.status",
                    "render":function(data, type, full, meta){
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                            "RUNNING":"green",
                            "default":"red"
                        }, data);
                    }
                },
                {
                    "data" :"info.health",
                    "render":function(data, type, full, meta){
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.LABEL_STATUS({
                            "Health":"green",
                            "default":"red"
                        }, data);
                    }
                },
                {
                    "data" :"info.startTime"
                },
                {
                    "data" :"info.maxJvm",
                    "render":function(data){
                        return data + "MB";
                    }
                },
                {
                    "data" :"info.jvmInfo.currentSize",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"jvmInfo", "currentSize", data + "MB");
                    }
                },
                {
                    "data" :"info.jvmInfo.freeSize",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"jvmInfo", "freeSize", data + "MB");
                    }
                },
                {
                    "data" :"info.jvmInfo.freePercent",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"jvmInfo", "freePercent", data + "%");
                    }
                },
                {
                    "data" :"info.queueInfo.maxThreadCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"queueInfo", "maxThreadCount", data);
                    }
                },
                {
                    "data" :"info.queueInfo.pendingCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"queueInfo", "pendingCount", data);
                    }
                },
                {
                    "data" :"info.queueInfo.idleCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"queueInfo", "idleCount", data);
                    }
                },
                {
                    "data" :"info.queueInfo.hoggingThreadCount",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"queueInfo", "hoggingThreadCount", data);
                    }
                },
                {
                    "data" :"info.queueInfo.throughput",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.DYNAMIC_DATA(full.viewId, 'weblogic',"queueInfo", "throughput", data);
                    }
                },
                {
                    "data" :"info.jdbcInfo",
                    "render":function(data, type, full, meta) {
                        var html = '';
                        var count = 0;
                        $.each(data, function(jdbcName, jdbcInfo) {
                            if (jdbcInfo.jdbcState == "Running") {
                                html += '<span class="layui-badge layui-bg-green">' + jdbcName + ':Running</span>&nbsp;';
                            } else {
                                html += '<span class="layui-badge">'+ jdbcName + ":" + jdbcInfo.jdbcState + '&nbsp;</span>';
                            }
                            count++;
                            if (count % 2 == 0) {
                                html += '<br>';
                            }

                        });

                        return html;
                    } //jdbcState
                },
                {
                    "data" :"info.jdbcInfo",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.COMPLEX_DYNAMIC_DATA(full.viewId, 'weblogic',"jdbcInfo", "activeConnectionsCurrentCount", data);
                    }  //activeConnectionsCurrentCount
                },
                {
                    "data" :"info.jdbcInfo",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.COMPLEX_DYNAMIC_DATA(full.viewId, 'weblogic',"jdbcInfo", "availableConnectionCount", data);
                    } //availableConnectionCount
                },
                {
                    "data" :"info.jdbcInfo",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.COMPLEX_DYNAMIC_DATA(full.viewId, 'weblogic',"jdbcInfo", "waitingForConnectionCurrentCount", data);
                    }  //waitingForConnectionCurrentCount
                },
                {
                    "data" :"info.jdbcInfo",
                    "render":function(data, type, full, meta) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.COMPLEX_DYNAMIC_DATA(full.viewId, 'weblogic',"jdbcInfo", "activeConnectionsHighCount", data);
                    }  //activeConnectionsHighCount
                },
                {
                    "data":"connectStatus",
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.CONNECT_STATUS(data);
                    }
                },
                {
                    "data":null,
                    "render":function(data) {
                        return DT_CONSTANT.DATA_TABLES.RENDERUTIL.ACTION_BAR(data.viewId, "weblogic", {
                        	"JVM":"rpm-monitoring-btn-other-jvm", 
                            "删除":SERVER_ACTION_BAR_BTN_DEL_CLASS,
                            "重连":SERVER_ACTION_BAR_BTN_RECONNECT_CLASS
                        });
                    }
                }
            ]
        },
        tableColumn:{
            "1":"ID",
            "2":"HOST",
            "3":"真实HOST",
            "4":"标签",
            "5":"用户名",
            "6":"节点名",
            "7":"运行状态",
            "8":"健康度",
            "9":"启动时间",
            "10":"JVM堆<br>最大值",
            "11":"JVM堆<br>当前使用",
            "12":"JVM堆<br>当前空闲",
            "13":"JVM堆<br>空闲百分比",
            "14":"活动线程总数",
            "15":"暂挂用户数",
            "16":"空闲线程数",
            "17":"独占线程数",
            "18":"吞吐量",
            "19":"JDBC当前<br>状态",
            "20":"JDBC当前<br>活动连接数",
            "21":"JDBC当前<br>可用连接数",
            "22":"JDBC当前<br>等待连接数",
            "23":"JDBC历史<br>最大连接数",
            "24":"连接状态",
            "25":"操作"
        },
        btnTool:[],
        alertSettingValue:{
            freePercent:{
                value:10,
                sign:"%",
                mode:"<"
            },
            pendingCount:{
                value:1,
                sign:"",
                mode:">"
            },
            waitingForConnectionCurrentCount:{
                value:1,
                sign:"",
                mode:">"
            },
            waitingForConnectionCurrentCount:{
                value:1,
                sign:"",
                mode:">"
            }
        },
        constant:{
            currentSize:"JVM堆当前使用大小[MB]",
            freeSize:"JVM堆当前空闲大小[MB]",
            freePercent:"JVM堆当前空闲百分比[%]",
            maxThreadCount:"活动线程总数",
            pendingCount:"暂挂用户请求数",
            idleCount:"空闲线程数",
            activeConnectionsCurrentCount:"JDBC当前活动连接数",
            availableConnectionCount:"JDBC当前可用连接数",
            waitingForConnectionCurrentCount:"JDBC当前等待连接数",
            hoggingThreadCount:"独占线程数",
            throughput:"吞吐量",
            activeConnectionsHighCount:"JDBC历史最大活动连接数",
            jdbcState:"JDBC当前状态"
        },
        propertyObject:{
            commonInfo:{
                health:[],
                runStatus:[]
            },
            jvmInfo:{
                currentSize:[],
                freeSize:[],
                freePercent:[]
            },
            queueInfo:{
                maxThreadCount:[],
                pendingCount:[],
                idleCount:[],
                throughput:[],
                hoggingThreadCount:[]
            },
            jdbcInfo:{
                jdbcState:{},
                activeConnectionsCurrentCount:{},
                availableConnectionCount:{},
                waitingForConnectionCurrentCount:{},
                activeConnectionsHighCount:{}
            }
        }
    }
};


/*********************************全局公共方法*******************************************/
/**
 * 获取地址栏参数
 * @param name
 * @returns
 */
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return decodeURIComponent(r[2]);
    return null;
}


/**
 * 验证返回值
 * @param json
 * @param callback
 */
function validatReturnJson(json, successCallback){
    if (json.returnCode == 0) {
        typeof successCallback == 'function' && successCallback();
    } else {
    	if (json.returnCode == 9) {
    		layer.alert(json.msg == null ? "登录失效" : json.msg, {icon:5, closeBtn: 0}, function(){
    			 window.location.href='./login.html';
    		});
    	} else {
    		layer.alert(json.msg == null ? "系统错误,请重试!" : json.msg, {icon:5});
    	}
        
    }
}

/**
 * 验证userKey
 * @param userKey
 */
function validateUserKey(userKey, hrefFlag) {
    $.post(VALIDATE_USER_KEY_URL, {userKey:userKey}, function(json){
    	if (json.returnCode == 0) {
    		layui.data('rmp', {
                key:'userKey',
                value:json.data.userKey
            });
    		$('.layui-nav-img').parent('a').append(json.data.userKey);
    		if (hrefFlag) {
                window.location.href = './index.html';
            }           
    	} else {
    		layer.alert(json.msg, {icon:5}, function(index) {
    			window.location.href = './login.html';
    			layer.close(index)
    		});
    	}
    });
}

/**
 * 复杂批量操作
 * @param opFun 批量操作方法,所有的批量操作都由此方法操作,需要更新totalCount, failCount, successCount
 * @param count 操作的数据总量
 * @param finishCallback 完成之后的回调操作,如果返回false则不会显示提示  参数 successCount,failCount
 */
function batchComplexOp (opFun, count, finishCallback) {
    if (typeof opFun != 'function') return false;

    var opCount = {
        total:0,
        success:0,
        fail:0
    };
    var loadIndex = layer.msg('正在批量操作中...', {icon:16, shade:0.4, time:999999});

    opFun(opCount);

    var intervalID = setInterval(function() {
        if (opCount.total == count) {
            clearInterval(intervalID);
            layer.close(loadIndex);
            var flag = true;
            if (typeof finishCallback == 'function') flag = callback(opCount.success, opCount.fail);
            flag && layer.msg("批量操作完成:成功记录" + opCount.success + "条,失败记录" + opCount.fail + '条!', {icon:1, time:2000});
        }
    }, 400);

}


/**
 * 批量操作方法
 * @param url   url 远程操作接口
 * @param table   对应Datatables实例
 * @param opName 操作方式名称 默认为 删除
 * @param idName 对应实体的ID名称
 * @param textName 名称的属性名
 * @param otherSendData 其他要随着ID发送的参数
 * @param callback 操作完成之后的回调
 * @returns {Boolean}
 */
function batchOp (url, opName, table, idName, textName, otherSendData, callback) {
    var checkdata =  $(table.table().container()).find("td > input:checked");
    if (checkdata.length < 1) {
        return false;
    }

    if (opName == null) {
        opName = "删除";
    }
    layer.confirm('确认' + opName + '选中的' + checkdata.length + '条记录?', {icon:0, title:'警告'}, function(index) {
        layer.close(index);
        var loadindex = layer.msg('正在进行批量' + opName + "...", {icon:16, time:60000, shade:0.35});
        var delCount = 0;
        var totalCount = 0;
        var errorTip = "";
        $.each(checkdata ,function(i, n) {
            var d = table.row($(n).parents('tr')).data();
            var objId = d[idName];//获取id
            var objName = d[textName];	//name属性为对象的名称
            var params = {id: objId};
            params[idName] = objId;//兼容id和实体对应的idName两种获取方式
            if (otherSendData != null && otherSendData instanceof Object) {
                $.each(otherSendData, function(i, n) {
                    params[i] = n;
                });
            }

            //layer.msg("正在" + opName + objName + "...", {time: 999999});
            $.ajax({
                type:"post",
                url:url,
                data:params,
                //async:false,
                success:function(data) {
                    totalCount++;
                    if(data.returnCode != 0) {
                        errorTip += "[" + objName + "]<br>";
                    }else{
                        opName == "删除" && table.row($(n).parents('tr')).remove().draw();
                        delCount++;
                    }
                }
            });
        });

        var intervalID = setInterval(function() {
            if (totalCount == checkdata.length) {
                clearInterval(intervalID);
                typeof callback == 'function' && callback();
                layer.close(loadindex);
                if (errorTip != "") {
                    errorTip = "在" + opName + "<br>" + errorTip + "时发生了错误<br>请执行单笔" + opName + "操作!";
                    layer.alert(errorTip, {icon:5}, function(index) {
                        layer.close(index);
                        layer.msg("共" + opName + delCount + "条数据!", {icon:1, time:2000});
                    });
                } else {
                    layer.msg("共" + opName + delCount + "条数据!", {icon:1, time:2000});
                }
            }
        }, 600);
    });
}

/**
 * 批量以代理的方式绑定监听事件
 * @param configs
 * @returns {$}
 */
$.fn.delegates = function(configs) {
    el = $(this[0]);
    for (var name in configs) {
        var value = configs[name];
        if (typeof value == 'function') {
            var obj = {};
            obj.click = value;
            value = obj;
        };
        for (var type in value) {
            el.delegate(name, type, value[type]);
        }
    }
    return this;
};

/**
 * 快速创建layer弹出层
 * 参数解释
 * @param title 标题
 * @param url  url地址或者html页面
 * @param w 宽度,默认值
 * @param h 高度,默认值
 * @param type 类型 1-页面层 2-frame层
 * @param success 成功打开之后的回调函数
 * @param cancel 右上角关闭层的回调函数
 * @param end 层销毁之后的回调
 * @param other 其他DT设置
 * @returns index
 */
function layer_show (title, url, w, h, type, success, cancel, end, other) {
    if (other == null) {
        other = {};
    }

    if (title == null || title == '') {
        title = false;
    };
    if (url == null || url == '') {
        url="404.html";
    };
    if (w == null || w == '' || w >= maxWidth) {//设置最大宽度
        w =	maxWidth * 0.9
    };
    if (h == null || h == '' || h >= maxHeight) {//设置最大高度
        h= (maxHeight * 0.86) ;
    };
    if (type == null || type == '') {
        type = 2;
    }
    index = layer.open($.extend(true, {
        type: type,
        area: [w + 'px', h + 'px'],
        fix: false, //不固定
        maxmin: false,
        shade:0.4,
        anim:5,
        shadeClose:true,
        title: title,
        content: url,
        success:success,
        cancel:cancel,
        end:end
    } ,other));
    return index;
}
/**
 * layui本地存储 存入数据
 * @param key
 * @param value 保存数据内容，如果是否对象类型则会转换成字符串
 * @param ifSession 是否从session Storage中获取,反之从local Storage中获取
 */
function saveLocalData(key, value, ifSession) {
    if (typeof value == 'object') {
        value = JSON.stringify(value);
    }
    if (ifSession) {
        layui.sessionData(tableName, {
            key: key
            ,value: value
        });
        return;
    }

    layui.data(tableName, {
        key: key
        ,value: value
    });
}

/**
 * layui本地存储 取出数据
 * @param key
 * @param parse 是否需要转换成json对象
 * @param ifSession 是否从session Storage中获取,反之从local Storage中获取
 */
function getLocalData(key, parse, ifSession) {
    var table;

    if (ifSession) {
        table = layui.sessionData(tableName);
    } else {
        table = layui.data(tableName);
    }

    var value = (table != null ? table[key] : null);
    if (parse && value != null) {
        value = JSON.parse(value);
    }
    return value;
}

/**
 * layui 本地存储 删除数据
 * @param key
 * @param ifSession 是否从session Storage中获取,反之从local Storage中获取
 */
function delLocalData(key, ifSession) {
    if (ifSession) {
        layui.sessionData(tableName, {
            key:key,
            remove:true
        });
        return;
    }

    layui.data(tableName, {
        key:key,
        remove:true
    });
}

/**
 * uuid生成
 * @returns
 */
function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
 
    var uuid = s.join("");
    return uuid;
}


/**
 * 显示等待标识
 * @param dom 显示等待标识的区域dom  jquery对象
 * @param flag true开启  flase关闭
 */
function showWaitFor(dom, flag) {
	if (!flag && window.waitForIntervalId != null) {		
		clearInterval(window.waitForIntervalId);
		window.waitForIntervalId = null;
		dom.text('');
	}
	
	if (flag) {
		dom.text('正在执行命令,请稍等/');
		var count = 1;
		var waiting = function(){
			if (count == 18) {
	    		dom.text("正在执行命令,请稍等/");
	    		count = 1;
	    		return;
	    	}
			var tips = dom.text();
			if (count % 2 == 0) {
	    		dom.text(tips.substring(0, (tips.length -1)) + "." + "/");
	    	} else {
	    		dom.text(tips.substring(0, (tips.length -1)) + "." + "\\");
	    	}	
			count++;
		}
		
		window.waitForIntervalId = setInterval(waiting, 1000);		
	}
}

/**
 * loading加载效果
 * @param flag true开启  false关闭
 * @param msg  提示语
 * @returns
 */
function loading(flag, msg) {
	if (flag) {
		globalLoadIndex = layer.msg(msg || '正在获取信息...', {icon:16, time:99999999, shade:0.4});		
	} else {
		globalLoadIndex != null && (layer.close(globalLoadIndex));
	}

}