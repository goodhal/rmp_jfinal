package com.dcits.constant;

/**
 * 缓存常量
 * @author xuwangcheng
 * @version 2018.2.5
 *
 */
public class ConstantCache {
	/**
	 * 缓存类型redis
	 */
	public static final String cache_type_redis = "redis";
	
	/**
	 * 主缓存,默认缓存所有内容
	 */
	public static final String cache_name_redis_main = "rmp_main";
}
