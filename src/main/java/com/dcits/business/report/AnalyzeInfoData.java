package com.dcits.business.report;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AnalyzeInfoData {
	
	private Integer viewId;
	private String host;
	private String serverType;
	private String tags;
	private JSONObject constant;
	
	private String beginTime;
	private String endTime;
	
	private List<AnalyzeItemData> items = new ArrayList<AnalyzeItemData>();

	public AnalyzeInfoData() {
		super();

	}
	
	public AnalyzeInfoData(Integer viewId, String host, String serverType, JSONObject constant) {
		super();
		this.viewId = viewId;
		this.host = host;
		this.serverType = serverType;
		this.constant = constant;
	}
	/**
	 * 分析数据
	 * @param itemRules 限定分析的类目
	 * @param infoData 保存的详细信息
	 */
	public void analyzeInfo(JSONArray itemRules, JSONObject infoData, Long beginTime, Long endTime) {
		infoData = infoData.getJSONObject("infoData").getJSONObject(this.serverType).getJSONObject(String.valueOf(this.viewId));
		
		JSONArray times = infoData.getJSONArray("time");
		this.beginTime = times.getString(0);
		this.endTime = times.getString(times.size() - 1);
		this.tags = infoData.getString("tags");

		for (Object itemName:itemRules) {					
			String[] names = itemName.toString().split("\\.");			
			JSONArray thisData = null;
			try {
				thisData = infoData.getJSONObject(names[0]).getJSONArray(names[1]);
				thisData.getDouble(0);
			} catch (Exception e) {
				continue;
			}
			
			AnalyzeItemData analyzeItemData = new AnalyzeItemData();
			analyzeItemData.setItemName(names[names.length - 1]);
			analyzeItemData.setItemMark(constant == null ? "" : constant.getString(names[names.length - 1]));
			analyzeItemData.analyzeData(thisData, times, beginTime, endTime);
			items.add(analyzeItemData);			
		}
	}
	
	public void setConstant(JSONObject constant) {
		this.constant = constant;
	}
	
	public JSONObject getConstant() {
		return constant;
	}
	
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	public String getTags() {
		if (tags == null) {
			return "";
		}
		return tags;
	}
	
	public Integer getViewId() {
		return viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getServerType() {
		return serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<AnalyzeItemData> getItems() {
		return items;
	}

	public void setItems(List<AnalyzeItemData> items) {
		this.items = items;
	}

}
