package com.dcits.business.report;


import com.alibaba.fastjson.JSONArray;
import com.dcits.tool.StringUtils;

public class AnalyzeItemData {
	
	private String itemName;
	private String itemMark;
	private String maxValue;
	private String minValue;
	private String avgValue;
	
	
	public void analyzeData(JSONArray datas, JSONArray times, Long beginTime, Long endTime) {
		if (datas == null) {
			return;
		}
		double max = 0.00;
		double min = 0.00;
		if (StringUtils.isNotEmpty(datas.get(0).toString())) {
			try {
				max = Double.parseDouble(datas.get(0).toString());
				min = Double.parseDouble(datas.get(0).toString());
			} catch (Exception e) {
				return;
			}			
		}
		
		double avg = 0;
		int count = 1;
		for (int i = 0;i < datas.size();i++) {
			if (StringUtils.isEmpty(datas.get(i).toString())) {
				continue;
			}
			
			long time = times.getDate(i).getTime();
			if ((beginTime != null && time < beginTime) || (endTime != null && time > endTime)) {
				continue;
			}
			
			if (max < Double.parseDouble(datas.get(i).toString())) {
				max = Double.parseDouble(datas.get(i).toString());
			}
			if (min > Double.parseDouble(datas.get(i).toString())) {
				min = Double.parseDouble(datas.get(i).toString());
			}
			
			avg += Double.parseDouble(datas.get(i).toString());
			count++;
		}
		
		maxValue = String.valueOf(max);
		minValue = String.valueOf(min);
		avgValue = String.format("%.2f", avg / count);	
	}
	
	
	public void setItemMark(String itemMark) {
		this.itemMark = itemMark;
	}
	
	public String getItemMark() {
		return itemMark;
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	public String getAvgValue() {
		return avgValue;
	}

	public void setAvgValue(String avgValue) {
		this.avgValue = avgValue;
	}
}
